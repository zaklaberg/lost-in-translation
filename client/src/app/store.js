import { configureStore } from '@reduxjs/toolkit'
import loginReducer from '../features/login/loginSlice'
import messageFooterReducer from '../features/messageFooter/messageFooterSlice'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'

export const history = createBrowserHistory()

export default configureStore({
  reducer: {
    router: connectRouter(history),
    login: loginReducer,
    messageFooter: messageFooterReducer
  },
  middleware: [routerMiddleware(history)]
});
