import React, { useState } from 'react'
import withAuth from '../auth/withAuth.js'
import styles from './Translate.module.css'
import SignTextbox from '../signTextbox/SignTextbox'
import { storeTranslation } from '../../api/translations'
import { selectUser } from '../login/loginSlice'
import { useDispatch, useSelector } from 'react-redux';
import { displayMessage } from '../messageFooter/messageFooterSlice'

const Translation = props => {
    const [ textToTranslate, setTextToTranslate ] = useState('')
    const userId = useSelector(selectUser).id
    const dispatch = useDispatch()
    let storeTranslationButton = null

    function onStoreTranslation() {
        storeTranslation(userId, textToTranslate)
        .catch(() => dispatch(displayMessage('Couldnt store translation for some reason, and we have no idea why. No. Idea.')))
    }

    function onTextToTranslateKeyUp(e) {
        const ENTER_KEY = 13
        if (!storeTranslationButton || e.which !== ENTER_KEY) {
            return
        }

        // Only the glorious USER can trigger :active. 
        // So, let's be a clevvr and work around it.
        storeTranslationButton.className = "active"
        setTimeout(() => {
            storeTranslationButton.className = ""
        }, 1)
        storeTranslationButton.click()
    }

    return (
        <div className={styles.translateContainer}>
            <input 
                type="text" 
                autoFocus
                value={textToTranslate}
                onChange={e => setTextToTranslate(e.target.value)}
                placeholder="Translate me!"
                onKeyUp={onTextToTranslateKeyUp} 
            />
            { 
                textToTranslate && 
                <>
                    <SignTextbox text={textToTranslate} />
                    <button 
                        ref={btn => storeTranslationButton = btn}
                        onClick={onStoreTranslation} 
                        className={styles.uiButton}
                    >
                        Store translation
                    </button>
                </>
            }
        </div>
    )
}

export default withAuth(Translation)