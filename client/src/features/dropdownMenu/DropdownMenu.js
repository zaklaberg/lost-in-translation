import React, { useState } from 'react'
import styles from './DropdownMenu.module.css'

const DropdownMenu = props => {
    const [ isVisible, setIsVisible ] = useState(false)
    const toggleMenu = () => setIsVisible( !isVisible )
    const optionAction = action => {
        toggleMenu()
        action()
    }
    const menuItems = props.optionsAndActions.map(({action, option}, idx) => <li onClick={() => optionAction(action)} key={idx}>{option}</li>)
    

    return (
        <div className={styles.dropdown}>
            <span onClick={toggleMenu} className={`${styles.profile} ${isVisible ? styles.active : ''} noselect`}>
                <p>{ props.title }</p>
                <span className="material-icons">
                face
                </span>
            </span>
            <div>
                <ul className={`${styles.menuList} ${isVisible ? styles.visible : ''}`}>
                    {menuItems}
                </ul>
            </div>
            
        </div>
    )
}

export default DropdownMenu