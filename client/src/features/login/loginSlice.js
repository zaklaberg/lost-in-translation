import { createSlice } from '@reduxjs/toolkit'

let userFromStorage = sessionStorage.getItem('_lit-user-ss')
if (userFromStorage) userFromStorage = JSON.parse(userFromStorage)
else userFromStorage = null
console.log('from storage', userFromStorage)

export const loginSlice = createSlice({
  name: 'login',
  initialState: {
    user: userFromStorage,
  },
  reducers: {
    // Why does this work !? A reducer should return the new state.. Look into it. 
    // Also, if this works, why does spreading onto state NOT work?
    login: (state, action) => {
      state.user = action.payload.user
    },
    logout: state => {
        state.user = ''
    },
  },
});

export const { login, logout } = loginSlice.actions;

export const selectUser = state => state.login.user;

export default loginSlice.reducer;
