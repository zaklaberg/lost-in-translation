import React, { useState } from 'react';
import styles from './Login.module.css';
import { Redirect } from 'react-router-dom';
import {
    login,
    selectUser
  } from './loginSlice';
import { push } from 'connected-react-router'
import { registerUser } from '../../api/user'
import { useDispatch, useSelector } from 'react-redux';
import { displayMessage } from '../messageFooter/messageFooterSlice'

const Login = () => {
    const [ username, setUsername ] = useState('')
    const dispatch = useDispatch()

    async function onFormSubmit(e) {
        e.preventDefault() // Just to silence a react warning
        try {
            // Add user to database
            const user = await registerUser(username)

            // Update user info in Redux
            dispatch(login({user}))
            
            // Update session storage with current user id
            sessionStorage.setItem('_lit-user-ss', JSON.stringify(user))
            
            // Everything done, move into the site proper
            dispatch(push('/translate'))
        }
        catch(e) {
            dispatch(displayMessage('Catastrophically failed to create user.'))
            return
        }
    }

    // Redux is initialized from session storage, so if there's a username there, we may be logged in. 
    // Redirect to private content - it will authenticate with the server anyway if someone messed with the session storage
    if (useSelector(selectUser)) {
        return <Redirect to='/translate' />
    }

    return (
        <div id={styles.loginContainer}>
            <span>
                <div id={styles.splash}>
                    <img src="/Logo.png" alt="Greetings!" />
                    <span>
                        <h3>Lost in Translation</h3>
                        <p>Get started</p>
                    </span>
                </div>

                { /* login form */ }
                <div className={styles.loginForm}>
                    <form method="POST" onSubmit={e => onFormSubmit(e)}>
                        <input autoFocus type="text" placeholder="What be your name?" onChange={e => setUsername(e.target.value)} />
                    </form>
                    <div>&nbsp;</div>
                </div>
            </span>
        </div>
    )
}

export default Login