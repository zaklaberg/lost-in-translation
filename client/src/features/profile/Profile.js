import React, { useState, useEffect } from 'react'
import withAuth from '../auth/withAuth.js'
import styles from './Profile.module.css'
import { fetchLastTranslations, deleteLastTranslations } from '../../api/translations'
import { selectUser } from '../login/loginSlice'
import SignTextbox from '../signTextbox/SignTextbox'
import { useDispatch, useSelector } from 'react-redux';
import { displayMessage } from '../messageFooter/messageFooterSlice'

const Profile = () => {
    const [translations, setTranslations] = useState(null)
    const [showLettersBelowSigns, setShowLettersBelowSigns] = useState(false)

    const dispatch = useDispatch()

    // We toggle this whenever translations are deleted, to re-fetch new translations if possible
    const [updateTranslations, setUpdateTranslations] = useState(false)
    const userId = useSelector(selectUser).id

    useEffect(() => {
        // API returns [] if there are no translations available - we turn it into null
        // because it's used to render conditionally
        function toNullIfEmpty(arr) {
            if (arr && arr.hasOwnProperty('length') && arr.length === 0) return null
            return arr
        }

        // This happens on logout. User is removed from Redux store, which triggers selectors, and then
        // this component is updated before withAuth - unfortunately. Not sure why -- HOC should render 
        // first, and thus subscribe first.
        if (!userId) {
            return
        }

        const translationsToFetch = 10

        fetchLastTranslations(userId, translationsToFetch)
            .then(trans => setTranslations(toNullIfEmpty(trans)))
            .catch(e => setTranslations(null))
    }, [userId, updateTranslations])

    function onTranslationDelete(amountToDelete) {
        deleteLastTranslations(userId, amountToDelete)
            .then(() => setTranslations(null))
            .then(() => setUpdateTranslations(prev => !prev))
            .catch(e => dispatch(displayMessage('We could not delete those translations. Really very sorry.')))
    }

    return (
        <div id={styles.profile}>
            { !translations && 
                <h4 className={styles.noTranslations} >
                    You currently do not have a SINGLE translation stored. Do something about it! NOW!
                </h4>
            }

            { translations && 
                <div>
                <header>
                    <h4>Your last {translations && translations.length} translations</h4>
                    <span className={styles.options}>
                        <button 
                            onClick={() => setShowLettersBelowSigns(state => !state)}
                            className={styles.uiButton}
                        >
                            { 
                                showLettersBelowSigns ? 'Hide letters below signs' : 'Show letters below signs'
                            }
                        </button>
                        <button 
                            onClick={onTranslationDelete.bind(null, translations.length)} 
                            className={styles.uiButton}
                        >
                            Delete shown translations
                        </button>
                        <button 
                            onClick={onTranslationDelete.bind(null, -1)} 
                            className={styles.uiButton}
                        >
                            Delete all translations
                        </button>
                    </span>
                </header>
                
                { 
                    translations.map((tr, i) => {
                        return (
                            <div key={i} className={styles.translatedPair} >
                                <p key={translations.length + i + 1} >{tr.text}</p>
                                <SignTextbox key={i} text={tr.text} showLettersBelowSigns={showLettersBelowSigns} />
                            </div>
                        )
                    })
                }
                </div>
            }
        </div>
    )
}

export default withAuth(Profile)