import React, { useEffect, useState } from 'react'
import styles from './SignTextbox.module.css'

/* Turns text into sign language by the use of black magic.
    Input: 
        [String] props.text 
*/

const SignTextbox = props => {
    const [translatedJSX, setTranslatedJSX] = useState('')

    // Update the sign translation whenever props.text change
    useEffect(() => {
        if (!props.text) return
        
        // Letters are used as keys into a map from [letter] -> [position in spritesheet]
        function genCharArray(charA, charZ) {
            const a = []
            let i = charA.charCodeAt(0)
            let j = charZ.charCodeAt(0)
            for (; i <= j; ++i) {
                a.push(String.fromCharCode(i))
            }
            return a;
        }

        // Map from letters to positions(offsets) in the spritesheet
        const letterToSpriteMap = [...genCharArray('a', 'z'), ' ']
        .map((letter, index) => {
            // The 48 is the size of each sign image in pixels. We currently only support this size of sign iamge.
            const increment = 48

            // The 8 here is because there are 8 images per row in our spritesheet.
            let offsetX = increment*(index % 8)
            let offsetY = increment*(Math.floor(index / 8))
            
            return { [letter]: { offsetX, offsetY } }
        })
        .reduce((acc, cv) => Object.assign(acc, cv), {})

        // Our spritesheet only has the alphabet, so we have to replace anything that's not a letter
        let representableText = [...props.text.toLowerCase().replace(/[^a-z\s]/gi, '')]
        
        // Turn the text into a list of divs with background image corresponding to the signs
        // If desired, also add the letter below each sign.
        const textOut = representableText.flatMap((letter, idx) => {
            const style = { backgroundPosition: `-${letterToSpriteMap[letter].offsetX}px -${letterToSpriteMap[letter].offsetY}px`}
            const sprite = <div key={idx} style={style} className={styles.bigSign}></div>
            if (props.showLettersBelowSigns) {
                return <div key={idx}>{sprite}<p>{letter}</p></div>
            }
            return sprite;
        })
        setTranslatedJSX(textOut)
    }, [props.text, props.showLettersBelowSigns])

    return <div className={styles.translateOutput}>{translatedJSX}</div>
}

export default SignTextbox