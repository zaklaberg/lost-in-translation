import { createSlice } from '@reduxjs/toolkit';
import MESSAGE_TYPE from './MessageTypes.constants'

export const messageFooterSlice = createSlice({
  // IMPORTANT! The [name] is NOT what you access the state with. 
  // Instead, it must be accessed by the name of the reducer you pass to configureStore / createStore
  name: 'messageFooter',
  initialState: {
    type: MESSAGE_TYPE.ERROR,
    text: '',
    previousMessageVisible: false
  },
  reducers: {
    displayMessage: (state, action) => {
      return {...state, ...action.payload, previousMessageVisible: true }
    },
    hideMessage: (state, action) => {
      return {type: state.type, text: '', previousMessageVisible: false }
    }
  },
})

export const { displayMessage, hideMessage } = messageFooterSlice.actions

export const selectMessage = state => state.messageFooter // returns {type, text, previousMessageVisible}

export default messageFooterSlice.reducer;
