const MESSAGE_TYPE = {
    INFO: 1,
    ERROR: 2,
    FRIENDLY: 3
}

export default MESSAGE_TYPE