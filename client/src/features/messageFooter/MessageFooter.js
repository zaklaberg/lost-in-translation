import React, { useState } from 'react'
import styles from './MessageFooter.module.css'
import { useDispatch, useSelector } from 'react-redux'
import {
    hideMessage,
    selectMessage
} from './messageFooterSlice'
import MESSAGE_TYPE from './MessageTypes.constants'


const MessageFooter = () => {
    const closeButtonPathDefault = '/close-button.png'
    const closeButtonPathHighlight = '/close-button-highlight.png'
    const [closeButtonPath, setCloseButtonPath] = useState(closeButtonPathDefault)

    const dispatch = useDispatch()
    const message = useSelector(selectMessage)
    
    if (!message || message.text === '') {
        return null
    }

    let visualClass = styles.error
    if (message.type === MESSAGE_TYPE.INFO) {
        visualClass = styles.info
    }
    else if (message.type === MESSAGE_TYPE.FRIENDLY) {
        visualClass = styles.friendly
    }
    
    return (
        <div id={styles.messageFooter} className={visualClass}>
            <p>{message.text}</p>
            { /* 'Tis bad to use anchors like this. I shouldnt do it. I should fix it. */ }
            <a>
                <img 
                    src={closeButtonPath}
                    alt="Close"
                    onClick={() => dispatch(hideMessage())}
                    onMouseEnter={() => setCloseButtonPath(closeButtonPathHighlight)}
                    onMouseLeave={() => setCloseButtonPath(closeButtonPathDefault)}
                />
            </a>
        </div>
    )
}

export default MessageFooter;
export { MESSAGE_TYPE }