import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {
    selectUser
} from '../login/loginSlice.js';
import Authenticating from './Authenticating'
import { userExists } from '../../api/user'

const withAuth = Component => props => {
    const user = useSelector(selectUser)

    let [isValidUser, setIsValidUser] = useState(false)
    let [isAuthenticating, setIsAuthenticating] = useState(true)
    
    // Make sure the user "token" is valid
    useEffect(() => {
        // A non-existing user is certainly not valid
        if (!user) {
            setIsAuthenticating(false)
            setIsValidUser(false)
            return
        }

        // Check with the server if that user id actually exists
        setIsAuthenticating(true)
        userExists(user.id)
        .then(resp => setIsValidUser(resp.exists))
        .catch(() => setIsValidUser(false))
        .finally(() => setIsAuthenticating(false))
    }, [user])

    if (isAuthenticating) {
        return <Authenticating />
    }
    else if (isValidUser) {
        return <Component {...props} />
    }
    else {
        return <Redirect to="/login" />
    }
}

export default withAuth