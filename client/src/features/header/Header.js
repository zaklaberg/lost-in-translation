import React from 'react';
import styles from './Header.module.css'
import { push } from 'connected-react-router'
import { useSelector, useDispatch } from 'react-redux';
import { selectUser, logout } from '../login/loginSlice'
import DropdownMenu from '../dropdownMenu/DropdownMenu.js'
import { deleteLastTranslations } from '../../api/translations'

const Header = () => {
    let user = useSelector(selectUser)
    const username = user ? user.name : ''
    const dispatch = useDispatch()
    const profileDropdownActions = [
        {
            action: () => { dispatch(push('/translate')) },
            option: 'Translate'
        },
        {
            action: () => { dispatch(push('/profile')) },
            option: 'Your translations'
        },
        {
            action: () => { 
                deleteLastTranslations(user.id)
                sessionStorage.removeItem('_lit-user-ss')
                dispatch(logout())
            },
            option: 'Logout'
        }
    ]

    return (
        <div id={styles.header}>
            <span>
                { username && 
                    <img src="/Logo.png" alt="Hi!" />
                }
                <h1>Lost in Translation</h1>
            </span>
            {
                username && 
                <DropdownMenu 
                    title={username}
                    optionsAndActions={profileDropdownActions} 
                />
            }
        </div>
    )
}

export default Header