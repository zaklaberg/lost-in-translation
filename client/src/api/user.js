import { SERVER_URL } from './constants'

// This function throws
async function registerUser(username) {
    const registerUserURL = `${SERVER_URL}/user/add`
    let resp = ''
    try {
        resp = await fetch(registerUserURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({name: username}),
        })

        // Server only returns { id: .... }
        resp = await resp.json()
        if (!resp || !resp.hasOwnProperty('id')) {
            throw new Error('Bad server response.')
        }
    }
    catch(e) {
        throw new Error(`Unable to add user, for reasons: ${e}`)
    }
    return { ...resp, name: username }
}

// This function returns a promise(why? because it's being used in a useEffect)
function userExists(userId) {
    return fetch(`${SERVER_URL}/user/${userId}/exists`)
        .then(resp => resp.json())
}

export { registerUser, userExists }