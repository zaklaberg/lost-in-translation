import { SERVER_URL } from './constants'

// If amount is not specified, fetches all translations for that user
function fetchLastTranslations(userId, amount = -1) {
    const urlAmount = amount === -1 ? '' : `last/${amount}`
    const urlFetch = `${SERVER_URL}/user/${userId}/translations/${urlAmount}`
    return fetch(urlFetch).then(resp => resp.json())
}

function deleteLastTranslations(userId, amount = -1) {
    const urlAmount = amount === -1 ? '' : `last/${amount}`
    const urlFetch = `${SERVER_URL}/user/${userId}/translations/${urlAmount}`
    return fetch(urlFetch, {
        method: 'DELETE'
    }).then(resp => resp.json())
}

function storeTranslation(userId, translation) {
    const url = `${SERVER_URL}/user/${userId}/translations/`
    return fetch(url, {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({text: translation}),
    }).then(resp => resp.json())
}


export { fetchLastTranslations, deleteLastTranslations, storeTranslation }