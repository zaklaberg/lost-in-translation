import React from 'react';
import {
  Route, 
  Switch
} from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import './normalize.css'
import './App.css';
import Header from './features/header/Header'
import Login from './features/login/Login'
import Translate from './features/translate/Translate'
import Profile from './features/profile/Profile'
import MessageFooter from './features/messageFooter/MessageFooter'
import { history } from './app/store.js'

function App() {
  return (
    <ConnectedRouter history={history}>
      <Header />
      <main>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/login" component={Login} />
          <Route path="/translate" component={Translate} />
          <Route path="/profile" component={Profile} />
        </Switch>
      </main>
      <MessageFooter />
    </ConnectedRouter>
  );
}

export default App;
