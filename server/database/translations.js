function addTranslation(user, translation) {
    user.get('translations')
        .push(translation)
        .write()
}

function getLastTranslations(user, amount = -1) {
    let translations = user.get('translations')
    if (amount !== -1) {
        translations = translations.takeRight(amount)
    }

    return translations.value()
}

function deleteLastTranslations(user, amount = -1) {
    // Remove everything
    if (amount === -1) {
        user.get('translations')
        .remove()
        .write()
        return
    }

    // Remove a particular set of translations
    // Note that takeRight().remove() does NOT work.
    let allTranslations = user.get('translations')
    const lastItemIds = allTranslations.takeRight(amount).map(tr => tr.id)  
    
    // Filter and .set is likely better - at least when [amount] is "large"
    lastItemIds.value().forEach(itemId => {
        allTranslations.remove({id: itemId}).write()
    })
}

exports.addTranslation = addTranslation
exports.getLastTranslations = getLastTranslations
exports.deleteLastTranslations = deleteLastTranslations