const db = require('./db.config')

function getUserById(userId) {
    let user = null
    try {
        user = db.get('users').find({id: userId})
        if (!user.value()) {
            throw new Error('User not found.')
        }
    }
    catch(e) {
        return null
    }
    return user
}

function getUserByName(name) {
    const user = db.get('users').find({name: name})
    if (user.value()) {
        return user
    }
    return null
}

function addUser(user) {
    db.get('users')
    .push(user)
    .write()
}

exports.getUserById = getUserById
exports.getUserByName = getUserByName
exports.addUser = addUser