const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

// Path in FileSync is relative to cwd
const adapter = new FileSync('./database/db.json')
const db = low(adapter)

const database = db.getState() 

if (!database) 
    db.defaults({ users: [] })

module.exports = db