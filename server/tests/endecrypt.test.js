const endec = require('../endecrypt')

it('Encrypts/decrypts correctly', () => {
    const text = "Well sir, I believe I am about to be encrypted! Nooooooo...."
    expect(endec.decrypt(endec.encrypt(text))).toBe(text)
})
