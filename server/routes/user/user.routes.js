const router = require('express').Router()
const endecrypt = require('../../endecrypt')
const nanoidLength = 8
const { nanoid } = require('nanoid')
const { 
    getUserById, 
    getUserByName, 
    addUser 
} = require('../../database/user')

router.get('/:userId/exists', (req, res) => {
    const data = { exists: false }
    const user = getUserById(endecrypt.decrypt(req.params.userId))
    if (user === null) {
        res.status(404).send(data)
    }
    else {
        data.exists = true
        res.status(200).send(data)
    }
})

// POST /api/user/:name
// Dual purpose; since no pw. If user does not exist, is created
// Returns encrypted userID
// Body is expected to be { name: value }
router.post('/add', (req, res) => {
    // If body is bad, return
    if (!req.body.hasOwnProperty('name')) {
        res.status(403).send({error: 'Body must contain {name: value}.'})
        return
    }

    // If user exists, return id
    let user = getUserByName(req.body.name)
    if (user !== null) {
        res.status(200).send({id: endecrypt.encrypt(user.value().id)})
        return
    }

    // User doesnt exist, add to database
    user = { name: req.body.name, id: nanoid(nanoidLength), translations: [] }
    addUser(user)
    
    // Return id and success!
    res.status(200).send({id: endecrypt.encrypt(user.id)})
})

router.use('/:userId/translations', require('./translations/translations.routes'))

module.exports = router