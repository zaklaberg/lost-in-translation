const router = require('express').Router({mergeParams: true})
const endecrypt = require('../../../endecrypt')
const nanoidLength = 8
const { nanoid } = require('nanoid')
const { getUserById } = require('../../../database/user')
const { 
    getLastTranslations, 
    addTranslation, 
    deleteLastTranslations 
} = require('../../../database/translations')

// Wrapper to exit with 404 if no user with corresponding id exists.
// If user exists, then user is added as third argument to handler.
// (This may possibly overwrite a third argument I never use. Should check!)
function exitOnInvalidUser(handler) {
    return (req, res) => {
        const userId = endecrypt.decrypt(req.params.userId)
        const user = getUserById(userId)
        if (user === null) {
            res.status(404).send({error: 'User not found.'})
            return
        }
        handler(req, res, user)
    }
}

// Return all translations for user
router.get('/', exitOnInvalidUser((req, res, user) => {
    const translations = getLastTranslations(user)
    res.send(translations)
}))

// Return last X translations
router.get('/last/:amount', exitOnInvalidUser((req, res, user) => {
    const translations = getLastTranslations(user, req.params.amount)
    res.status(200).send(translations)
}))

// POST /api/translations
// Add a new translation
// Body is expected to be { text: value }
router.post('/', exitOnInvalidUser((req, res, user) => {
    // Return on invalid body
    let translation = req.body
    if (!translation || !translation.hasOwnProperty('text')) {
        res.status(400).send({error: "Body must contain {text: value}."})
        return
    }

    // Add id, for future reasons (delete only particular translations, for example)
    translation = { ...translation, id: nanoid(nanoidLength) }

    // Store translation in database
    addTranslation(user, translation)

    res.status(201).send({success: true})
}))

// DELETE /api/translations/last/:amount
// Deletes the last X translations
router.delete('/last/:amount', exitOnInvalidUser((req, res, user) => {
    deleteLastTranslations(user, req.params.amount)
    res.status(200).send({success: true})
}))

// DELETE /api/translations
// Deletes all translations
router.delete('/', exitOnInvalidUser((req, res, user) => {
    deleteLastTranslations(user)
    res.status(200).send({success: true})
}))

module.exports = router