const fs = require('fs')
const crypto = require('crypto')

function storeCipherData() {
    const algorithm = 'aes-256-cbc'
    const key = crypto.randomBytes(32)
    const iv = crypto.randomBytes(16)
    let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv)
    const dummyText = "Look at me, I'm encrypted!"
    let encrypted = cipher.update(dummyText);
    encrypted = Buffer.concat([encrypted, cipher.final()]);

    const cipherData = JSON.stringify({ algorithm, iv: iv.toString('hex'), key: key.toString('hex'), dummyText: encrypted.toString('hex') })
    fs.writeFileSync('cipherData.json', cipherData)
}


let cipherData = {}
if (!fs.existsSync('cipherData.json')) {
    console.log('woopdiedoop')
    storeCipherData()
}
cipherData = JSON.parse(fs.readFileSync('cipherData.json'))
const key = Buffer.from(cipherData.key, 'hex')
const iv = Buffer.from(cipherData.iv, 'hex')
const algorithm = cipherData.algorithm

exports.decrypt = text => {
    let encryptedText = Buffer.from(text, 'hex');
    let decipher = crypto.createDecipheriv(algorithm, key, iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

exports.encrypt = text => {
    let cipher = crypto.createCipheriv(algorithm, key, iv)
    let encrypted = cipher.update(text)
    encrypted = Buffer.concat([encrypted, cipher.final()])
    return encrypted.toString('hex')
}