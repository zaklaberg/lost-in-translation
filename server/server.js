const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const { PORT = 3000 } = process.env

// Create server
const app = express()
app.use(bodyParser.json())
app.use(cors())

app.get('/', (req, res) => {
    return res.status(500).send('Stay away, you nosy little....')
})

app.use('/api/user', require('./routes/user/user.routes'))

app.listen(PORT, () => console.log(`listening on port ${PORT}`))