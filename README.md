# A marvelous website for translating regular, boring text into fancy, great sign language.

## Usage:
- npm install (in both client and server folder)
- npm start in server folder
- npm start in client folder (This is so the server can run on port 3000. If it runs on any other port, modify the URL in /api/constants.js)

# That's it! Enjoy fancy sign language! 
